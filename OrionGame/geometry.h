#pragma once
#include <GL/glew.h>


// Creates a VAO with 4 vertices, forming a rectangle of 
// 1 unit length. 
// The width spans on the x-axis from -0.5 to 0.5 (left to right).
// The height spans on the y-axis from -0.5 to 0.5 (bottom to top).
GLuint createUnitRectVAO();

#pragma once
#include "level_gameplay.h"
#include "game.h"
#include <gl_helper.h>

class LevelGamePlay {

public:
	LevelGamePlay(Game* game, int level);

	void update(float ft, Game* game);
	void render(float ft, Game* game);
	void drawTextAt(int x, int y, FVAO* rectVAO, const Game& game, TextObject* text, int zLayer, glm::vec4 color);
	void renderVAO(float rt, FVAO vao, FShader* shader, FTexture* texture, Camera* camera);


private:
	FVAO* _shipModelVAO;
	FVAO* _debugGridVAO;
	bool _paused = false;
	float _angleYaw;
	Game* _game = nullptr;

};

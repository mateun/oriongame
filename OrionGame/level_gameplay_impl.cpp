#include "level_gameplay.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL.h>

LevelGamePlay::LevelGamePlay(Game* game, int level) : _game(game) {

	_shipModelVAO = f_importModelFromFile(game->assetBasePath + "/ship.obj");
	_debugGridVAO = createDebugGridVAO(100);
	_angleYaw = 0;
}

void LevelGamePlay::update(float ft, Game* game) {
	for (int i = 0; i < game->frameEvents->size(); i++) {
		SDL_Event event = game->frameEvents->at(i);
		if (event.type == SDL_KEYDOWN) {
			if (event.key.keysym.sym == SDLK_SPACE) {
				_paused = !_paused;
			}
		}
	}

	if (!_paused) {
		_angleYaw += 0.01f;
	}
}

void LevelGamePlay::renderVAO(float rt, FVAO vao, FShader* shader, FTexture* texture, Camera* camera) {
	glBindVertexArray(vao.vaoId);
	glUseProgram(shader->_shaderId);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->texId);

	glm::mat4 scale = glm::scale(glm::mat4(1), glm::vec3(0.5, 0.5, 0.5));
	glm::mat4 rot = glm::rotate(glm::mat4(1), _angleYaw, glm::vec3(0, 1, 0));
	glm::mat4 trans = glm::translate(glm::mat4(1), glm::vec3(-2, 0.0, -2));
	glm::mat4 model = trans * scale * rot;

	glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
	glUniformMatrix4fv(7, 1, false, glm::value_ptr(_game->debugCamera.viewMatrix));
	glUniformMatrix4fv(8, 1, false, glm::value_ptr(_game->debugCamera.projMatrix));
	glUniformMatrix4fv(9, 1, false, glm::value_ptr(_game->lightCamera.projMatrix));
	glUniformMatrix4fv(10, 1, false, glm::value_ptr(_game->lightCamera.viewMatrix));

	glUniform1i(100, 1);
	glUniform4fv(101, 1, glm::value_ptr(glm::vec4(0.3, 0.2, 1, 1)));
	glUniform1i(102, 1);
	glUniform3fv(103, 1, glm::value_ptr(_game->lightDirection));
	glUniform1i(104, 0);


	glDrawElements(GL_TRIANGLES, _shipModelVAO->numberOfVertices, GL_UNSIGNED_INT, 0);
}

void LevelGamePlay::render(float rt, Game* game) {
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawDebugGrid(_debugGridVAO, game->defaultShader, game->debugCamera);

	drawTextAt(_game->fpsText->w / 2, _game->screenHeight - _game->fpsText->h / 2 - 24, _game->rectVAO, *_game, _game->updateTimeText, -2, glm::vec4(1, 1, 1, 1));

	SDL_GL_SwapWindow(game->window);

}


void LevelGamePlay::drawTextAt(int x, int y, FVAO* rectVAO, const Game& game, TextObject* text, int zLayer, glm::vec4 color) {
	glBindVertexArray(rectVAO->vaoId);
	glUseProgram(game.defaultShader._shaderId);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, text->texture.texId);

	glm::mat4 scale = glm::scale(glm::mat4(1), glm::vec3(text->w, text->h, 1));
	glm::mat4 trans = glm::translate(glm::mat4(1), glm::vec3(x, y, zLayer));
	glm::mat4 model = trans * scale;
	glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
	glUniformMatrix4fv(7, 1, false, glm::value_ptr(game.uiCamera.viewMatrix));
	glUniformMatrix4fv(8, 1, false, glm::value_ptr(game.uiCamera.projMatrix));
	glUniform1i(100, 0);
	glUniform4fv(101, 1, glm::value_ptr(color));
	glUniform1i(102, 0);
	glUniform1i(104, 1);
	glDrawElements(GL_TRIANGLES, rectVAO->numberOfVertices, GL_UNSIGNED_INT, 0);


}
#pragma once
#include <gl_helper.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <thread>
#include <condition_variable>
#include <queue>
#include <functional>



enum GameState {
	MAIN_MENU,
	IN_LEVEL
};






struct Game {
	Game(const std::string basePath, int w, int h, SDL_Window* win) : assetBasePath(basePath), screenWidth(w), screenHeight(h), window(win)
	{
		initFreeType();
		initDefaultShader();
		initTextures();
		initCameras();
		initRectVAO();
		
		gameState = MAIN_MENU;

		numberOfThreadsAvailable = std::thread::hardware_concurrency();
		SDL_Log("number of threads available: %d", numberOfThreadsAvailable);

		for (int i = 0; i < numberOfThreadsAvailable; i++) {
			threadPool.push_back(std::thread(&Game::infiniteLoopFunc, this, i));
		}
		
	}

	void initFreeType() {
		f_initFreeType(&ftLibrary);
		f_createFontFace(&ftLibrary, &debugFontFace, assetBasePath + "/tahoma.ttf");
		f_setCharSize(&debugFontFace, 8);

		fpsText = new TextObject(debugFontFace, 20);
		frameTimeText = new TextObject(debugFontFace, 13);
		startButtonText = new TextObject(debugFontFace, 11);
		updateTimeText = new TextObject(debugFontFace, 20);
		startButtonText->setText("START GAME");

	}

	void initTextures();

	void initDefaultShader() {
		std::string vs = readFile(assetBasePath + "vshader.glsl");
		std::string fs = readFile(assetBasePath + "fshader.glsl");
		f_createShader(vs, fs, &defaultShader);
	}

	void initCameras() {
		glm::mat4 mat_proj = glm::perspective<float>(glm::pi<float>()* 0.16, 1.3, 0.01, 500);
		glm::mat4 mat_view = glm::lookAt(glm::vec3(0, 5, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		gamePlayCamera.projMatrix = mat_proj;
		gamePlayCamera.viewMatrix = mat_view;
		debugCamera.projMatrix = mat_proj;
		debugCamera.viewMatrix = mat_view;

		glm::mat4 mat_proj_ortho = glm::ortho<float>(0, screenWidth, 0, screenHeight, 0.01, 500);
		glm::mat4 view_ortho = glm::lookAt<float>(glm::vec3{ 0, 0, 1 }, { 0,0,0 }, { 0, 1, 0 });
		uiCamera.projMatrix = mat_proj_ortho;
		uiCamera.viewMatrix = view_ortho;


		this->lightDirection = glm::vec3(-50.0, -0.0, -0.0);
		glm::vec3 lcamPos = glm::vec3(-0, 30, 0);
		glm::vec3 lcamFwd = lcamPos + ((lightDirection) * 10.0f);
		lightFrustumLeft = -55;
		lightFrustumRight = 55;
		lightFrustumBottom = -55;
		lightFrustumTop = 55;
		lightFrustumNear = 0.01;
		lightFrustumFar = 60;
		lightCamera = { lcamPos, lcamFwd, glm::lookAt(lcamPos, lcamFwd, glm::vec3(0, 1, 0)) };
		lightCamera.projMatrix = glm::ortho<float>(lightFrustumLeft, lightFrustumRight, lightFrustumBottom, lightFrustumTop, lightFrustumNear, lightFrustumFar);

	}


	void initRectVAO() {
		rectVAO = new FVAO();
		f_createVAO(rectVAO);

		std::vector<float> pos = {
			-0.5, -0.5, -2,
			0.5, -0.5, -2,
			0.5, 0.5, -2,
			-0.5, 0.5,-2
		};
		FVBuffer pb;
		f_createVertexBuffer(pos, &pb);
		f_setVertexAttribute(0, GL_FLOAT, 3);

		std::vector<float> uv = {
			0, 0,
			1, 0,
			1, 1,
			0, 1
		};
		FVBuffer uvb;
		f_createVertexBuffer(uv, &uvb);
		f_setVertexAttribute(1, GL_FLOAT, 2);

		std::vector<uint32_t> indices = {
			0, 1, 2,
			0, 2, 3
		};
		FIBuffer ib;
		f_createIndexBuffer(indices, &ib);
		rectVAO->numberOfVertices = 6;
		f_unbindVAO();


	}

	void infiniteLoopFunc(int n);
	void addJob(std::function<void()> newJob);

	std::string assetBasePath;

	FShader defaultShader;

	FVAO* rectVAO;

	FTexture texRoad;
	FTexture texAmmoLabel;
	FTexture texButtonStart;
	FTexture texButtonQuit;
	FTexture texStarPigSplash;

	Camera gamePlayCamera;
	Camera debugCamera;
	Camera uiCamera;
	Camera lightCamera;

	glm::vec3 lightDirection = glm::vec3(1);
	float lightFrustumLeft;
	float lightFrustumRight;
	float lightFrustumTop;
	float lightFrustumBottom;
	float lightFrustumNear;
	float lightFrustumFar;

	FT_Library ftLibrary;
	FT_Face debugFontFace;

	TextObject* fpsText;
	TextObject* frameTimeText;
	TextObject* startButtonText;
	TextObject* updateTimeText;

	int screenWidth;
	int screenHeight;

	GameState gameState;
	SDL_Window* window;

	std::vector<SDL_Event>* frameEvents;

	int numberOfThreadsAvailable = 0;

	std::vector<std::thread> threadPool;
	std::condition_variable condition;
	std::mutex queueMutex;
	std::queue < std::function<void()>> jobQueue;

	int frame = 0;

	bool terrainAvailable = false;
	bool terrainBuilding = false;

};



// OrionGame.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <SDL.h>
#include <GL/glew.h>
#include <gl_helper.h>
#include "game.h"
#include <iomanip>
#include <sstream>
#define STB_IMAGE_IMPLEMENTATION
#include "pngimport.h"
#include "level_gameplay.h"

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "openGL32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "GlHelperLib.lib")
#pragma comment(lib, "freetyped.lib")
#pragma comment(lib, "assimp-vc142-mtd.lib")

LevelGamePlay* createLevelGamePlayForLevel(int level, Game* game);

static LevelGamePlay* currentLevelGamePlay = nullptr;

void printFoobar() {
	SDL_Log("printing foobar!!\n");
}

void buildHugeTerrain(Game* game) {
	using namespace std::chrono_literals;
	SDL_Log("starting terrain building\n");
	game->terrainBuilding = true;
	for (int i = 0; i < 100; i++) {
		std::this_thread::sleep_for(1000ms);
		SDL_Log("progress: %d%%", i);
	}
	
	SDL_Log("finished terrain building\n");
	game->terrainAvailable = true;
	game->terrainBuilding = false;
}

void Game::addJob(std::function<void()> newJob) {
	{
		std::unique_lock<std::mutex> lock(queueMutex);
		jobQueue.push(newJob);
	}
	condition.notify_one();
}


void Game::infiniteLoopFunc(int n) {
	SDL_Log("starting worker thread! %d\n", n);
	std::function<void()> job;
	while (true) {
		{
			std::unique_lock<std::mutex> lock(queueMutex);
			//std::queue<std::function<void()>> tmpQ = this->jobQueue;
			condition.wait(lock, [this] { return !jobQueue.empty(); });
			job = jobQueue.front();
			jobQueue.pop();
		}
		SDL_Log("doing work in thread: %d\n", n);
		job();
	}

}

void Game::initTextures() {
	int imageChannels, w, h;

	// Road texture
	uint8_t* imgBytes = stbi_load((assetBasePath + "../../OrionGame/assets/road_seamless.jpg").c_str(), &w, &h, &imageChannels, 4);
	f_createTextureRGBA(w, h, imgBytes, &texRoad);

	imgBytes = stbi_load((assetBasePath + "../../OrionGame/assets/ammo_label.png").c_str(), &w, &h, &imageChannels, 4);
	f_createTextureRGBA(w, h, imgBytes, &texAmmoLabel);

	imgBytes = stbi_load((assetBasePath + "../../OrionGame/assets/button_start.png").c_str(), &w, &h, &imageChannels, 4);
	f_createTextureRGBA(w, h, imgBytes, &texButtonStart);

	imgBytes = stbi_load((assetBasePath + "../../OrionGame/assets/button_quit.png").c_str(), &w, &h, &imageChannels, 4);
	f_createTextureRGBA(w, h, imgBytes, &texButtonQuit);

	imgBytes = stbi_load((assetBasePath + "../../OrionGame/assets/starpig_splash.png").c_str(), &w, &h, &imageChannels, 4);
	f_createTextureRGBA(w, h, imgBytes, &texStarPigSplash);

}

void drawUITextAt(int x, int y, FVAO* rectVAO, const Game& game, TextObject* text, int zLayer, glm::vec4 color) {
	glBindVertexArray(rectVAO->vaoId);
	glUseProgram(game.defaultShader._shaderId);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, text->texture.texId);

	glm::mat4 scale = glm::scale(glm::mat4(1), glm::vec3(text->w, text->h, 1));
	glm::mat4 trans = glm::translate(glm::mat4(1), glm::vec3(x, y, zLayer));
	glm::mat4 model = trans * scale;
	glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
	glUniformMatrix4fv(7, 1, false, glm::value_ptr(game.uiCamera.viewMatrix));
	glUniformMatrix4fv(8, 1, false, glm::value_ptr(game.uiCamera.projMatrix));
	glUniform1i(100, 0);
	glUniform4fv(101, 1, glm::value_ptr(color));
	glUniform1i(102, 0);
	glUniform1i(104, 1);
	glDrawElements(GL_TRIANGLES, rectVAO->numberOfVertices, GL_UNSIGNED_INT, 0);


}

void drawVAOOrtho(int x, int y, FVAO* vao, const Game& game, FTexture texture, int zLayer) {
	glBindVertexArray(vao->vaoId);
	glUseProgram(game.defaultShader._shaderId);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.texId);

	glm::mat4 scale = glm::scale(glm::mat4(1), glm::vec3(texture.w, texture.h, 1));
	glm::mat4 trans = glm::translate(glm::mat4(1), glm::vec3(x, y, zLayer));
	glm::mat4 model = trans * scale;
	glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
	glUniformMatrix4fv(7, 1, false, glm::value_ptr(game.uiCamera.viewMatrix));
	glUniformMatrix4fv(8, 1, false, glm::value_ptr(game.uiCamera.projMatrix));
	glUniform1i(100, 0);
	glUniform4fv(101, 1, glm::value_ptr(glm::vec4(1, 0, 0, 1)));
	glUniform1i(102, 0);
	glUniform1i(104, 0);
	glDrawElements(GL_TRIANGLES, vao->numberOfVertices, GL_UNSIGNED_INT, 0);


}

void drawVAO(FVAO* vao, const Game& game, FTexture texture) {
	glBindVertexArray(vao->vaoId);
	glUseProgram(game.defaultShader._shaderId);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, game.texRoad.texId);

	glm::mat4 scale = glm::scale(glm::mat4(1), glm::vec3(10, 1, 10));
	glm::mat4 rot = glm::rotate(glm::mat4(1), -glm::pi<float>() / 2.0f, glm::vec3(1, 0, 0));
	glm::mat4 trans = glm::translate(glm::mat4(1), glm::vec3(0, 0.0, -15));
	glm::mat4 model = trans *  scale * rot;

	glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
	glUniformMatrix4fv(7, 1, false, glm::value_ptr(game.debugCamera.viewMatrix));
	glUniformMatrix4fv(8, 1, false, glm::value_ptr(game.debugCamera.projMatrix));
	glUniformMatrix4fv(9, 1, false, glm::value_ptr(game.lightCamera.projMatrix));
	glUniformMatrix4fv(10, 1, false, glm::value_ptr(game.lightCamera.viewMatrix));

	glUniform1i(100, 0);
	glUniform4fv(101, 1, glm::value_ptr(glm::vec4(1, 0, 0, 1)));
	glUniform1i(102, 0);
	glUniform3fv(103, 1, glm::value_ptr(game.lightDirection));
	glUniform1i(104, 0);


	glDrawElements(GL_TRIANGLES, vao->numberOfVertices, GL_UNSIGNED_INT, 0);
	
}



void initGL(SDL_Window* window) {
	SDL_GLContext glContext = SDL_GL_CreateContext(window);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GLContext ctx = SDL_GL_CreateContext(window);
	SDL_GL_SetSwapInterval(1);

	GLenum r = glewInit();
	if (r != GLEW_OK) {
		exit(2);
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

}

void drawUpdatedFPSText(Game& game, int fps) {
	
	
	game.fpsText->setText("FPS: " + std::to_string(fps));

}

void updateUI(float ft, float updateTime, Game& game) {
	if (game.gameState == MAIN_MENU) {
		int mx, my;
		if (SDL_GetMouseState(&mx, &my) & SDL_BUTTON_LEFT) {
			if (mx > 350 && mx < 470 && my > 280 && my < 350) {
				game.gameState = GameState::IN_LEVEL;
				currentLevelGamePlay = createLevelGamePlayForLevel(1, &game);
			}
		}
	}

}



void update(float ft, float updateTime, Game& game) {
	game.frame++;
	if (game.frame % 50 == 0) {
		//game.addJob(printFoobar);
	}

	if (game.frame % 50 == 0 && game.terrainAvailable == false && game.terrainBuilding == false) {
		game.addJob(std::bind(buildHugeTerrain, &game));
	}


	updateUI(ft, updateTime, game);

	std::stringstream stream;
	stream.str(std::string());
	stream << std::fixed << std::setprecision(2) << updateTime;
	std::string s2 = "UpdateTime: " + stream.str();
	game.updateTimeText->setText(s2);

	if (game.gameState == GameState::IN_LEVEL && currentLevelGamePlay != nullptr) {
		currentLevelGamePlay->update(ft, &game);
	}
}

void render(float rt, Game& game) {
	

	if (game.gameState == GameState::MAIN_MENU ) {

		glClearColor(0.3, 0.1, 0.1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		if (game.gameState == MAIN_MENU) {
			drawVAOOrtho(game.screenWidth / 2, game.screenHeight / 2, game.rectVAO, game, game.texStarPigSplash, -5.5);
			drawVAOOrtho(game.screenWidth / 2, game.screenHeight / 2, game.rectVAO, game, game.texButtonStart, -4);
			drawVAOOrtho(game.screenWidth / 2, game.screenHeight / 2 - 80, game.rectVAO, game, game.texButtonQuit, -4);
		}
		//drawUITextAt(game.fpsText->w / 2, game.screenHeight - game.fpsText->h / 2 - 2, rectVAO, game, game.fpsText, -2, glm::vec4(1, 1, 1, 1));
		drawUITextAt(game.fpsText->w / 2, game.screenHeight - game.fpsText->h / 2 - 24, game.rectVAO, game, game.updateTimeText, -2, glm::vec4(1, 1, 1, 1));

		SDL_GL_SwapWindow(game.window);
	}

	
	if (game.gameState == GameState::IN_LEVEL && currentLevelGamePlay != nullptr) {
		currentLevelGamePlay->render(rt, &game);
	}

	
}


LevelGamePlay* createLevelGamePlayForLevel(int level, Game* game) {
	// TODO different levels instantiate different gameplay classes.
	LevelGamePlay* levelGameplay = new LevelGamePlay(game, level);
	return levelGameplay;
}

int main(int argc, char** args) {

	if (SDL_Init(SDL_INIT_VIDEO) == -1) {
		SDL_Log("error initializing. %s", SDL_GetError());
	}

	SDL_Window* window = SDL_CreateWindow("test 0.0.1", 300, 100, 800, 600, SDL_WINDOW_OPENGL);
	initGL(window);

	Game game(std::string(SDL_GetBasePath() + std::string("../../OrionGame/assets/")), 800, 600, window);
	
	//FVAO* rectVAO = createRectVAO();
	FVAO* debugGridVAO = createDebugGridVAO(200);

	bool running = true;

	
	FTimer frameTimer1;
	FTimer updateTimer;
	frameTimer1.start();
	std::vector<SDL_Event> frameEvents;
	while (running) {
		frameTimer1.stop();
		frameTimer1.start();
		
		frameEvents.clear();
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				running = false;
			}
			frameEvents.push_back(event);
		}
		
		updateTimer.start();
		game.frameEvents = &frameEvents;
		update(frameTimer1.lastDurationInMillis(), updateTimer.lastDurationInMillis(), game);
		updateTimer.stop();

		render(frameTimer1.lastDurationInMillis(), game);
		
		
		
	}

	return 0;
}


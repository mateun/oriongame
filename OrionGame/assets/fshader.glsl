#version 430

layout(binding = 0) uniform sampler2D colorTex;
layout(binding = 1) uniform sampler2D shadowTex;

layout(location = 100) uniform bool useSingleColor;
layout(location = 101) uniform vec4 singleColor;
layout(location = 102) uniform bool lit = true;
layout(location = 103) uniform vec3 lightDir;
layout(location = 104) uniform bool isText = false;

in vec2 fs_uvs;
in vec4 fs_normal;
out vec4 color;
in vec4 fs_light_pos;

vec4 ambientTerm() {
	return vec4(0.05, 0.07, 0.04, 1);
}

vec4 diffuseTerm() {
	//vec4 lightDir = normalize(vec4(1, 0.5, 0.75, 0));
	vec4 ld = normalize(vec4(-lightDir, 0));
	float d = max(0, dot(fs_normal, ld));
	return vec4(0.8, 0.85, 0.8, 1) * d;
}

vec4 shadow(vec4 incol) {

	vec3 lpt = (fs_light_pos.xyz) / (fs_light_pos.w) * 0.5 + 0.5;
	float lz = texture(shadowTex, lpt.xy).r;
	if (lpt.z > (lz + 0.001)) {
		return vec4(0.15, 0.29, 0.26, incol.a);
	}
	else {
		return vec4(1, 1, 1, incol.a);
	}

}

void main() {

	if (isText) {
		vec4 textColor = texture(colorTex, fs_uvs);
		color = vec4(singleColor.xyz, textColor.a);
	}
	else {
		if (useSingleColor) {
			color = singleColor;
			if (lit) {
				color = color * (ambientTerm() + diffuseTerm());
			}
		}
		else
		{
			color = texture(colorTex, fs_uvs);
			if (lit) {
				color = color * (ambientTerm() + diffuseTerm());
				color *= shadow(color);


			}
			else {
				color = color;
			}



		}

	}

	

}

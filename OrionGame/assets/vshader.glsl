#version 430

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uvs;
layout(location = 2) in vec3 normal;

layout(location = 6) uniform mat4 model;
layout(location = 7) uniform mat4 view;
layout(location = 8) uniform mat4 proj;

layout(location = 9) uniform mat4 lightProj;
layout(location = 10) uniform mat4 lightView;

out vec2 fs_uvs;
out vec4 fs_normal;
out vec4 fs_light_pos;

void main() {

	gl_Position = proj * view * model * vec4(pos, 1);
	fs_uvs = uvs;
	fs_uvs.y = 1 - fs_uvs.y;
	//fs_normal = model * vec4(normal, 0);
	fs_normal = normalize(model * vec4(normal, 0.0));
	fs_light_pos = lightProj * lightView * model * vec4(pos, 1);
}

#include "rendering.h"


void drawVAOFlatColored(GLuint vao, GLuint shader, glm::mat4 projectionMatrix, glm::mat4 viewMatrix, glm::mat4 worldMatrix, glm::vec3 color)
{
	glBindVertexArray(vao);
	glUseProgram(shader);

	glUniformMatrix4fv(6, 1, false, glm::value_ptr(worldMatrix));
	glUniformMatrix4fv(7, 1, false, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(8, 1, false, glm::value_ptr(projectionMatrix));
	//glUniformMatrix4fv(9, 1, false, glm::value_ptr(_game->lightCamera.projMatrix));
	//glUniformMatrix4fv(10, 1, false, glm::value_ptr(_game->lightCamera.viewMatrix));

	// TODO handle the lighting
	glUniform1i(100, 1);
	glUniform4fv(101, 1, glm::value_ptr(glm::vec4(0.3, 0.2, 1, 1)));
	glUniform1i(102, 1);
	//glUniform3fv(103, 1, glm::value_ptr(_game->lightDirection));
	glUniform1i(104, 0);


	// 
	//glDrawElements(GL_TRIANGLES, _shipModelVAO->numberOfVertices, GL_UNSIGNED_INT, 0);

}
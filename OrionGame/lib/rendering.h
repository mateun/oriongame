#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>



void drawVAOFlatColored(GLuint vao, GLuint shader, glm::mat4 projectionMatrix, glm::mat4 viewMatrix, glm::mat4 worldMatrix, glm::vec3 color);
void drawVAOVertexColored(GLuint vao, glm::mat4 projectionMatrix, glm::mat4 viewMatrix, glm::mat4 worldMatrix);
void drawVAOTextured(GLuint vao, glm::mat4 projectionMatrix, glm::mat4 viewMatrix, glm::mat4 worldMatrix, GLuint texture);
